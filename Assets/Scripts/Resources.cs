﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class Resources : MonoBehaviour {

	public bool letsPlay = false;
	public int teamTutorialResourceOne;
	public int teamTutorialResourceTwo;
	public int teamTutorialResourceThree;
	public int teamTutorialResourceFour;

	public int teamOneResourceOne;
	public int teamOneResourceTwo;
	public int teamOneResourceThree;
	public int teamOneResourceFour;

	public int teamTwoResourceOne;
	public int teamTwoResourceTwo;
	public int teamTwoResourceThree;
	public int teamTwoResourceFour;

	public int teamThreeResourceOne;
	public int teamThreeResourceTwo;
	public int teamThreeResourceThree;
	public int teamThreeResourceFour;

	public int teamFourResourceOne;
	public int teamFourResourceTwo;
	public int teamFourResourceThree;
	public int teamFourResourceFour;

	public int teamTutorialResourceOneLastTick;
	public int teamTutorialResourceTwoLastTick;
	public int teamTutorialResourceThreeLastTick;
	public int teamTutorialResourceFourLastTick;

	public int teamOneResourceOneLastTick;
	public int teamOneResourceTwoLastTick;
	public int teamOneResourceThreeLastTick;
	public int teamOneResourceFourLastTick;

	public int teamTwoResourceOneLastTick;
	public int teamTwoResourceTwoLastTick;
	public int teamTwoResourceThreeLastTick;
	public int teamTwoResourceFourLastTick;

	public int teamThreeResourceOneLastTick;
	public int teamThreeResourceTwoLastTick;
	public int teamThreeResourceThreeLastTick;
	public int teamThreeResourceFourLastTick;

	public int teamFourResourceOneLastTick;
	public int teamFourResourceTwoLastTick;
	public int teamFourResourceThreeLastTick;
	public int teamFourResourceFourLastTick;

	public int teamTutorialResourceOneDifference;
	public int teamTutorialResourceTwoDifference;
	public int teamTutorialResourceThreeDifference;
	public int teamTutorialResourceFourDifference;

	public int teamOneResourceOneDifference;
	public int teamOneResourceTwoDifference;
	public int teamOneResourceThreeDifference;
	public int teamOneResourceFourDifference;

	public int teamTwoResourceOneDifference;
	public int teamTwoResourceTwoDifference;
	public int teamTwoResourceThreeDifference;
	public int teamTwoResourceFourDifference;

	public int teamThreeResourceOneDifference;
	public int teamThreeResourceTwoDifference;
	public int teamThreeResourceThreeDifference;
	public int teamThreeResourceFourDifference;

	public int teamFourResourceOneDifference;
	public int teamFourResourceTwoDifference;
	public int teamFourResourceThreeDifference;
	public int teamFourResourceFourDifference;

	public TeamSizes teamSizes;

	public GameManager gameManager;
	public DatabaseData data; //the json holder for resources;

	[System.Serializable]
	public struct DatabaseData
	{
		public int team0Resource1;
		public int team0Resource2;
		public int team0Resource3;
		public int team0Resource4;

		public int team1Resource1;
		public int team1Resource2;
		public int team1Resource3;
		public int team1Resource4;

		public int team2Resource1;
		public int team2Resource2;
		public int team2Resource3;
		public int team2Resource4;

		public int team3Resource1;
		public int team3Resource2;
		public int team3Resource3;
		public int team3Resource4;

		public int team4Resource1;
		public int team4Resource2;
		public int team4Resource3;
		public int team4Resource4;


	}
		
	// Use this for initialization
	[System.Serializable]
	public struct TeamSizes
	{
		public int teamOneSize;
		public int teamTwoSize;
		public int teamThreeSize;
		public int teamFourSize;

	}
	// Use this for initialization
	void Start () {
		gameManager = this.GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
	public IEnumerator getTeamSize()
	{
		//Step One, retreive the team sizes and store them in a json
		string requestString = "http://grad.uat2017.com/common/php/teamprint.php";
		WWW www = new WWW (requestString);

		yield return www;//wait for the response
		Debug.Log(www.text);
		teamSizes = JsonUtility.FromJson<TeamSizes> (www.text); // Take the database page's text and fit it into our json object

		//Step Two, update the game manager;

		gameManager.teamOneSize = teamSizes.teamOneSize;
		gameManager.teamTwoSize = teamSizes.teamTwoSize;
		gameManager.teamThreeSize = teamSizes.teamThreeSize;
		gameManager.teamFourSize = teamSizes.teamFourSize;

	}



	public IEnumerator startOfGame()
	{
		// STEP TWO		Getting the data from the php page
		string requestString = "http://grad.uat2017.com/common/php/testtest.php";
		WWW www = new WWW (requestString);

		yield return www;//wait for the response
		Debug.Log(www.text);
		data = JsonUtility.FromJson<DatabaseData> (www.text); // Take the database page's text and fit it into our json object

		teamTutorialResourceOneLastTick = data.team0Resource1; 
		teamTutorialResourceTwoLastTick = data.team0Resource2;
		teamTutorialResourceThreeLastTick = data.team0Resource3;
		teamTutorialResourceFourLastTick = data.team0Resource4;

		teamOneResourceOneLastTick = data.team1Resource1;
		teamOneResourceTwoLastTick = data.team1Resource2;
		teamOneResourceThreeLastTick = data.team1Resource3;
		teamOneResourceFourLastTick = data.team1Resource4;

		teamTwoResourceOneLastTick = data.team2Resource1;
		teamTwoResourceTwoLastTick = data.team2Resource2;
		teamTwoResourceThreeLastTick = data.team2Resource3;
		teamTwoResourceFourLastTick = data.team2Resource4;

		teamThreeResourceOneLastTick = data.team3Resource1;
		teamThreeResourceTwoLastTick = data.team3Resource2;
		teamThreeResourceThreeLastTick = data.team3Resource3;
		teamThreeResourceFourLastTick = data.team3Resource4;

		teamFourResourceOneLastTick = data.team4Resource1;
		teamFourResourceTwoLastTick = data.team4Resource2;
		teamFourResourceThreeLastTick = data.team4Resource3;
		teamFourResourceFourLastTick = data.team4Resource4;
	
	
	}



	public IEnumerator GetWebData()
	{
		if(letsPlay == true)
		{
			// STEP ONE		IEnumerator sets the old resource value to the current resource value
			teamTutorialResourceOneLastTick = teamTutorialResourceOne; 
			teamTutorialResourceTwoLastTick = teamTutorialResourceTwo;
			teamTutorialResourceThreeLastTick = teamTutorialResourceThree;
			teamTutorialResourceFourLastTick = teamTutorialResourceFour;

			teamOneResourceOneLastTick = teamOneResourceOne;
			teamOneResourceTwoLastTick = teamOneResourceTwo;
			teamOneResourceThreeLastTick = teamOneResourceThree;
			teamOneResourceFourLastTick = teamOneResourceFour;

			teamTwoResourceOneLastTick = teamTwoResourceOne;
			teamTwoResourceTwoLastTick = teamTwoResourceTwo;
			teamTwoResourceThreeLastTick = teamTwoResourceThree;
			teamTwoResourceFourLastTick = teamTwoResourceFour;

			teamThreeResourceOneLastTick = teamThreeResourceOne;
			teamThreeResourceTwoLastTick = teamThreeResourceTwo;
			teamThreeResourceThreeLastTick = teamThreeResourceThree;
			teamThreeResourceFourLastTick = teamThreeResourceFour;

			teamFourResourceOneLastTick = teamFourResourceOne;
			teamFourResourceTwoLastTick = teamFourResourceTwo;
			teamFourResourceThreeLastTick = teamFourResourceThree;
			teamFourResourceFourLastTick = teamFourResourceFour;



			// STEP TWO		Getting the data from the php page
			string requestString = "http://grad.uat2017.com/common/php/testtest.php";
			WWW www = new WWW (requestString);

			yield return www;//wait for the response
			Debug.Log(www.text);
			data = JsonUtility.FromJson<DatabaseData> (www.text); // Take the database page's text and fit it into our json object





			//STEP THREE		setting the current resource values equal to the recieved JSON values
			teamTutorialResourceOne = data.team0Resource1; 
			teamTutorialResourceTwo = data.team0Resource2;
			teamTutorialResourceThree = data.team0Resource3;
			teamTutorialResourceFour = data.team0Resource4;

			teamOneResourceOne = data.team1Resource1; 
			teamOneResourceTwo = data.team1Resource2; 
			teamOneResourceThree = data.team1Resource3; 
			teamOneResourceFour = data.team1Resource4; 

			teamTwoResourceOne = data.team2Resource1; 
			teamTwoResourceTwo = data.team2Resource2; ;
			teamTwoResourceThree = data.team2Resource3; ;
			teamTwoResourceFour = data.team2Resource4; ;

			teamThreeResourceOne = data.team3Resource1; ;
			teamThreeResourceTwo = data.team3Resource2; ;
			teamThreeResourceThree = data.team3Resource3; ;
			teamThreeResourceFour = data.team3Resource4; ;

			teamFourResourceOne = data.team4Resource1; ;
			teamFourResourceTwo = data.team4Resource2; ;
			teamFourResourceThree = data.team4Resource3; ;
			teamFourResourceFour = data.team4Resource4; ;

			//STEP FOUR		Update the difference values, taking into account team size weighting
			teamTutorialResourceOneDifference = teamTutorialResourceOne - teamTutorialResourceOneLastTick;
			teamTutorialResourceTwoDifference = teamTutorialResourceTwo - teamTutorialResourceTwoLastTick;
			teamTutorialResourceThreeDifference = teamTutorialResourceThree - teamTutorialResourceThreeLastTick;
			teamTutorialResourceFourDifference = teamTutorialResourceFour - teamTutorialResourceFourLastTick;

			teamOneResourceOneDifference = (teamOneResourceOne - teamOneResourceOneLastTick) * gameManager.totalPlayers / gameManager.teamOneSize ;
			teamOneResourceTwoDifference = (teamOneResourceTwo - teamOneResourceTwoLastTick) * gameManager.totalPlayers / gameManager.teamOneSize ;
			teamOneResourceThreeDifference = (teamOneResourceThree - teamOneResourceThreeLastTick) * gameManager.totalPlayers / gameManager.teamOneSize ;
			teamOneResourceFourDifference = (teamOneResourceFour - teamOneResourceFourLastTick) * gameManager.totalPlayers / gameManager.teamOneSize ;

			teamTwoResourceOneDifference = (teamTwoResourceOne - teamTwoResourceOneLastTick) * gameManager.totalPlayers / gameManager.teamTwoSize ;
			teamTwoResourceTwoDifference = (teamTwoResourceTwo - teamTwoResourceTwoLastTick) * gameManager.totalPlayers / gameManager.teamTwoSize ;
			teamTwoResourceThreeDifference = (teamTwoResourceThree - teamTwoResourceThreeLastTick) * gameManager.totalPlayers / gameManager.teamTwoSize ;
			teamTwoResourceFourDifference = (teamTwoResourceFour - teamTwoResourceFourLastTick) * gameManager.totalPlayers / gameManager.teamTwoSize ;

			teamThreeResourceOneDifference = (teamThreeResourceOne - teamThreeResourceOneLastTick) * gameManager.totalPlayers / gameManager.teamThreeSize ;
			teamThreeResourceTwoDifference = (teamThreeResourceTwo - teamThreeResourceTwoLastTick) * gameManager.totalPlayers / gameManager.teamThreeSize ;
			teamThreeResourceThreeDifference = (teamThreeResourceThree - teamThreeResourceThreeLastTick) * gameManager.totalPlayers / gameManager.teamThreeSize ;
			teamThreeResourceFourDifference = (teamThreeResourceFour - teamThreeResourceFourLastTick) * gameManager.totalPlayers / gameManager.teamThreeSize ;

			teamFourResourceOneDifference = (teamFourResourceOne - teamFourResourceOneLastTick) * gameManager.totalPlayers / gameManager.teamFourSize ;
			teamFourResourceTwoDifference = (teamFourResourceTwo - teamFourResourceTwoLastTick) * gameManager.totalPlayers / gameManager.teamFourSize ;
			teamFourResourceThreeDifference = (teamFourResourceThree - teamFourResourceThreeLastTick) * gameManager.totalPlayers / gameManager.teamFourSize ;
			teamFourResourceFourDifference = (teamFourResourceFour - teamFourResourceFourLastTick) * gameManager.totalPlayers / gameManager.teamFourSize;
		}
	}


	public void addResourceOne()
	{
		teamTutorialResourceOneDifference = 1;
		teamOneResourceOneDifference = 1;
		teamTwoResourceOneDifference = 1;
		teamThreeResourceOneDifference = 1;
		teamFourResourceOneDifference = 1;

		teamTutorialResourceTwoDifference = 0;
		teamOneResourceTwoDifference = 0;
		teamTwoResourceTwoDifference = 0;
		teamThreeResourceTwoDifference = 0;
		teamFourResourceTwoDifference = 0;

		teamTutorialResourceThreeDifference = 0;
		teamOneResourceThreeDifference = 0;
		teamTwoResourceThreeDifference = 0;
		teamThreeResourceThreeDifference = 0;
		teamFourResourceThreeDifference = 0;

		teamTutorialResourceFourDifference = 0;
		teamOneResourceFourDifference = 0;
		teamTwoResourceFourDifference = 0;
		teamThreeResourceFourDifference = 0;
		teamFourResourceFourDifference = 0;

	}

	public void addResourceTwo()
	{

		teamTutorialResourceOneDifference = 0;
		teamOneResourceOneDifference = 0;
		teamTwoResourceOneDifference = 0;
		teamThreeResourceOneDifference = 0;
		teamFourResourceOneDifference = 0;

		teamTutorialResourceTwoDifference = 1;
		teamOneResourceTwoDifference = 1;
		teamTwoResourceTwoDifference = 1;
		teamThreeResourceTwoDifference = 1;
		teamFourResourceTwoDifference = 1;

		teamTutorialResourceThreeDifference = 0;
		teamOneResourceThreeDifference = 0;
		teamTwoResourceThreeDifference = 0;
		teamThreeResourceThreeDifference = 0;
		teamFourResourceThreeDifference = 0;

		teamTutorialResourceFourDifference = 0;
		teamOneResourceFourDifference = 0;
		teamTwoResourceFourDifference = 0;
		teamThreeResourceFourDifference = 0;
		teamFourResourceFourDifference = 0;

	}

	public void addResourceThree()
	{
		teamTutorialResourceOneDifference = 0;
		teamOneResourceOneDifference = 0;
		teamTwoResourceOneDifference = 0;
		teamThreeResourceOneDifference = 0;
		teamFourResourceOneDifference = 0;

		teamTutorialResourceTwoDifference = 0;
		teamOneResourceTwoDifference = 0;
		teamTwoResourceTwoDifference = 0;
		teamThreeResourceTwoDifference = 0;
		teamFourResourceTwoDifference = 0;

		teamTutorialResourceThreeDifference = 1;
		teamOneResourceThreeDifference = 1;
		teamTwoResourceThreeDifference = 1;
		teamThreeResourceThreeDifference = 1;
		teamFourResourceThreeDifference = 1;

		teamTutorialResourceFourDifference = 0;
		teamOneResourceFourDifference = 0;
		teamTwoResourceFourDifference = 0;
		teamThreeResourceFourDifference = 0;
		teamFourResourceFourDifference = 0;
	}

	public void addResourceFour()
	{
		teamTutorialResourceOneDifference = 0;
		teamOneResourceOneDifference = 0;
		teamTwoResourceOneDifference = 0;
		teamThreeResourceOneDifference = 0;
		teamFourResourceOneDifference = 0;

		teamTutorialResourceTwoDifference = 0;
		teamOneResourceTwoDifference = 0;
		teamTwoResourceTwoDifference = 0;
		teamThreeResourceTwoDifference = 0;
		teamFourResourceTwoDifference = 0;

		teamTutorialResourceThreeDifference = 0;
		teamOneResourceThreeDifference = 0;
		teamTwoResourceThreeDifference = 0;
		teamThreeResourceThreeDifference = 0;
		teamFourResourceThreeDifference = 0;

		teamTutorialResourceFourDifference = 1;
		teamOneResourceFourDifference = 1;
		teamTwoResourceFourDifference = 1;
		teamThreeResourceFourDifference = 1;
		teamFourResourceFourDifference = 1;

	}






	public void UpdateResources()
	{
		teamTutorialResourceOne = teamTutorialResourceOneLastTick;
		teamTutorialResourceTwo = teamTutorialResourceTwoLastTick;
		teamTutorialResourceThree = teamTutorialResourceThreeLastTick;
		teamTutorialResourceFour = teamTutorialResourceFourLastTick;

		teamOneResourceOne = teamOneResourceOneLastTick;
		teamOneResourceTwo = teamOneResourceTwoLastTick;
		teamOneResourceThree = teamOneResourceThreeLastTick;
		teamOneResourceFour = teamOneResourceFourLastTick;

		teamTwoResourceOne = teamTwoResourceOneLastTick;
		teamTwoResourceTwo = teamTwoResourceTwoLastTick;
		teamTwoResourceThree = teamTwoResourceThreeLastTick;
		teamTwoResourceFour = teamTwoResourceFourLastTick;

		teamThreeResourceOne = teamThreeResourceOneLastTick;
		teamThreeResourceTwo = teamThreeResourceTwoLastTick;
		teamThreeResourceThree = teamThreeResourceThreeLastTick;
		teamThreeResourceFour = teamThreeResourceFourLastTick;

		teamFourResourceOne = teamFourResourceOneLastTick;
		teamFourResourceTwo = teamFourResourceTwoLastTick;
		teamFourResourceThree = teamFourResourceThreeLastTick;
		teamFourResourceFour = teamFourResourceFourLastTick;



		//Update the current values from the database






		//Set the change variables

		teamTutorialResourceOneDifference = teamTutorialResourceOne - teamTutorialResourceOneLastTick;
		teamTutorialResourceTwoDifference = teamTutorialResourceTwo - teamTutorialResourceTwoLastTick;
		teamTutorialResourceThreeDifference = teamTutorialResourceThree - teamTutorialResourceThreeLastTick;
		teamTutorialResourceFourDifference = teamTutorialResourceFour - teamTutorialResourceFourLastTick;

		teamOneResourceOneDifference = teamOneResourceOne - teamOneResourceOneLastTick;
		teamOneResourceTwoDifference = teamOneResourceTwo - teamOneResourceTwoLastTick;
		teamOneResourceThreeDifference = teamOneResourceThree - teamOneResourceThreeLastTick;
		teamOneResourceFourDifference = teamOneResourceFour - teamOneResourceFourLastTick;

		teamTwoResourceOneDifference = teamTwoResourceOne - teamTwoResourceOneLastTick;
		teamTwoResourceTwoDifference = teamTwoResourceTwo - teamTwoResourceTwoLastTick;
		teamTwoResourceThreeDifference = teamTwoResourceThree - teamTwoResourceThreeLastTick;
		teamTwoResourceFourDifference = teamTwoResourceFour - teamTwoResourceFourLastTick;

		teamThreeResourceOneDifference = teamThreeResourceOne - teamThreeResourceOneLastTick;
		teamThreeResourceTwoDifference = teamThreeResourceTwo - teamThreeResourceTwoLastTick;
		teamThreeResourceThreeDifference = teamThreeResourceThree - teamThreeResourceThreeLastTick;
		teamThreeResourceFourDifference = teamThreeResourceFour - teamThreeResourceFourLastTick;

		teamFourResourceOneDifference = teamFourResourceOne - teamFourResourceOneLastTick;
		teamFourResourceTwoDifference = teamFourResourceTwo - teamFourResourceTwoLastTick;
		teamFourResourceThreeDifference = teamFourResourceThree - teamFourResourceThreeLastTick;
		teamFourResourceFourDifference = teamFourResourceFour - teamFourResourceFourLastTick;
	}



	public void testDataChange()
	{		
		teamTutorialResourceOneDifference = 1;
		teamTutorialResourceTwoDifference = 1;
		teamTutorialResourceThreeDifference = 1;
		teamTutorialResourceFourDifference = 1;

		teamOneResourceOneDifference = 1;
		teamOneResourceTwoDifference =1 ;
		teamOneResourceThreeDifference = 1;
		teamOneResourceFourDifference = 1;

		teamTwoResourceOneDifference = 1;
		teamTwoResourceTwoDifference = 1;
		teamTwoResourceThreeDifference = 1;
		teamTwoResourceFourDifference = 1;

		teamThreeResourceOneDifference = 1;
		teamThreeResourceTwoDifference = 1;
		teamThreeResourceThreeDifference = 1;
		teamThreeResourceFourDifference = 1;

		teamFourResourceOneDifference = 1;
		teamFourResourceTwoDifference = 1;
		teamFourResourceThreeDifference = 1;
		teamFourResourceFourDifference = 1;
	}
}
