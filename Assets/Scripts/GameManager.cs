﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public int fistPlaceTeam;
	public int secondPlaceTeam;
	public int thirdPlaceTeam;
	public int fourthPlaceTeam;
	public bool teamOnePlaced;
	public bool teamTwoPlaced;
	public bool teamThreePlaced;
	public bool teamFourPlaced;

	public Sprite teamOneSprite;
	public Sprite teamTwoSprite;
	public Sprite teamThreeSprite;
	public Sprite teamFourSprite;

	public MoveRight teamTutorialStudentMR;
	public MoveRight teamOneStudentMR;
	public MoveRight teamTwoStudentMR;
	public MoveRight teamThreeStudentMR;
	public MoveRight teamFourStudentMR;
	public Resources resources;
	public enum GameState { TutorialGame, RealGame};
	public GameState gameState;
	public GameObject mainCamera;
	public Obstacle[] teamTutorialObstacles;
	public Obstacle[] teamOneObstacles;
	public Obstacle[] teamTwoObstacles;
	public Obstacle[] teamThreeObstacles;
	public Obstacle[] teamFourObstacles;

	public int teamTutorialCurrentObstacle;
	public int teamOneCurrentObstacle;
	public int teamTwoCurrentObstacle;
	public int teamThreeCurrentObstacle;
	public int teamFourCurrentObstacle;

	public float resourceSendTime;
	public GameObject[] resourceArray;
	public GameObject[] resourceSpawners;
	public GameObject[] playerTargets;
	public GameObject tempObject;

	public int teamOneSize;
	public int teamTwoSize;
	public int teamThreeSize;
	public int teamFourSize;
	public int totalPlayers;
	public float updateTimer;
	public float updateTime = 0.5f;
	public static GameManager _GameManager = null;

	public float timerMaxTime = 30.0f;
	public float teamTutorialTimer;
	public float teamOneTimer;
	public float teamTwoTimer;
	public float teamThreeTimer;
	public float teamFourTimer;
	void Awake()
	{
		if (_GameManager == null) 
		{
			_GameManager = this;
		}
		DontDestroyOnLoad (gameObject);
	}

	void Start () 
	{

		resources = this.GetComponent<Resources> ();

	}
	
	// Update is called once per frame
	void Update () 
	{
		totalPlayers = teamOneSize + teamTwoSize + teamThreeSize + teamFourSize;

		if (gameState == GameState.TutorialGame) //TUTORIAL GAMEPLAY
		{

			if (resources.letsPlay == true) 
			{
				teamTutorialTimer += Time.deltaTime;
			}

			mainCamera.GetComponent<Transform> ().position = new Vector3 (-16.88f, -18.9f, -3f);
			if (teamTutorialCurrentObstacle == teamTutorialObstacles.Length) 
			{
				gameState = GameState.RealGame;
			}

			if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourcesLeft <= 0) //When an obstacle is completer
			{
				teamTutorialTimer = 0;
				teamTutorialObstacles [teamTutorialCurrentObstacle].gameObject.SetActive (false);
				//teamTutorialCurrentObstacle++; // increment the current obstacle 

				if (teamTutorialCurrentObstacle == 0) 
				{
					teamTutorialCurrentObstacle = 1;
					Debug.Log("Incremented teamTutorialObstacle to Obstacle: " + teamTutorialCurrentObstacle);
				}

				else if (teamTutorialCurrentObstacle == 1) 
				{
					teamTutorialCurrentObstacle = 2;
					Debug.Log("Incremented teamTutorialObstacle to Obstacle: " + teamTutorialCurrentObstacle);
				}

				else if (teamTutorialCurrentObstacle == 2) 
				{
					teamTutorialCurrentObstacle = 3;
					Debug.Log("Incremented teamTutorialObstacle to Obstacle: " + teamTutorialCurrentObstacle);
				}

				else if (teamTutorialCurrentObstacle == 3) 
				{
					teamTutorialCurrentObstacle = 4;
					Debug.Log("Incremented teamTutorialObstacle to Obstacle: " + teamTutorialCurrentObstacle);
				}
				Debug.Log("Incremented teamTutorialObstacle to Obstacle: " + teamTutorialCurrentObstacle);
				teamTutorialStudentMR.shouldMove = true;
				Debug.Log ("Unfroze TT Student");

			}

			if (Input.GetKeyDown (KeyCode.Space)) 
			{
				resources.letsPlay = true;
				StartCoroutine (resources.startOfGame ());
				StartCoroutine(resources.GetWebData());
				testTutorialFunction ();
			}

			if (updateTimer <= 0) 
			{
				StartCoroutine (resources.getTeamSize());
				StartCoroutine (resources.GetWebData ());
				testTutorialFunction ();

				updateTimer = updateTime;
			} 
			else 
			{
				updateTimer -= Time.deltaTime;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				resources.addResourceOne ();
				doObstacleMath ();
			}

			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				resources.addResourceTwo ();
				doObstacleMath();
			}

			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				resources.addResourceThree ();
				doObstacleMath();
			}

			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				resources.addResourceFour ();
				doObstacleMath();
			}

		}

		if (gameState == GameState.RealGame) //REAL GAMEPLAY
		{
			mainCamera.GetComponent<Transform> ().position = new Vector3 (6.09f, 0.2f, -5.2f);
			teamOneTimer += Time.deltaTime;
			teamTwoTimer += Time.deltaTime;
			teamThreeTimer += Time.deltaTime;
			teamFourTimer += Time.deltaTime;
			if (teamOneCurrentObstacle < teamOneObstacles.Length && teamOneObstacles [teamOneCurrentObstacle].resourcesLeft <= 0) //When an obstacle is completer
			{
				teamOneTimer = 0;
				teamOneObstacles [teamOneCurrentObstacle].gameObject.SetActive (false);
				teamOneCurrentObstacle++; // increment the current obstacle 
				teamOneStudentMR.shouldMove = true;
				Debug.Log ("Unfroze T1 Student");

			}

			if (teamTwoCurrentObstacle < teamTwoObstacles.Length && teamTwoObstacles [teamTwoCurrentObstacle].resourcesLeft <= 0) //When an obstacle is completer
			{
				teamTwoTimer = 0;
				teamTwoObstacles [teamTwoCurrentObstacle].gameObject.SetActive (false);
				teamTwoCurrentObstacle++; // increment the current obstacle 
				teamTwoStudentMR.shouldMove = true;
				Debug.Log ("Unfroze T2 Student");

			}

			if (teamThreeCurrentObstacle < teamThreeObstacles.Length && teamThreeObstacles [teamThreeCurrentObstacle].resourcesLeft <= 0) //When an obstacle is completer
			{
				teamThreeTimer = 0;
				teamThreeObstacles [teamThreeCurrentObstacle].gameObject.SetActive (false);
				teamThreeCurrentObstacle++; // increment the current obstacle 
				teamThreeStudentMR.shouldMove = true;
				Debug.Log ("Unfroze T3 Student");

			}

			if (teamFourCurrentObstacle < teamFourObstacles.Length && teamFourObstacles [teamFourCurrentObstacle].resourcesLeft <= 0) //When an obstacle is completer
			{
				teamFourTimer = 0;
				teamFourObstacles [teamFourCurrentObstacle].gameObject.SetActive (false);
				teamFourCurrentObstacle++; // increment the current obstacle 
				teamFourStudentMR.shouldMove = true;
				Debug.Log ("Unfroze T4 Student");

			}

			if (Input.GetKeyDown (KeyCode.Space)) 
			{
				//testFunction ();
				StartCoroutine(resources.GetWebData());
				doObstacleMath ();
			}

			if (updateTimer <= 0) 
			{
				StartCoroutine (resources.getTeamSize());
				StartCoroutine(resources.GetWebData());
				doObstacleMath ();
				updateTimer = updateTime;
			} 
			else 
			{
				updateTimer -= Time.deltaTime;
			}

			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				resources.addResourceOne ();
				doObstacleMath ();
			}

			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				resources.addResourceTwo ();
				doObstacleMath();
			}

			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				resources.addResourceThree ();
				doObstacleMath();
			}

			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				resources.addResourceFour ();
				doObstacleMath();
			}


			if (teamOnePlaced == true && teamTwoPlaced == true && teamThreePlaced == true && teamFourPlaced == true)
			{
				UnityEngine.SceneManagement.SceneManager.LoadScene (1);
			}
		}
	}

	public void tutorialData()
	{
		
	}
	public void updateData()
	{
		resources.UpdateResources (); // Pull new data.

	}


	public void testFunction()
	{
		resources.testDataChange();

		doObstacleMath ();
	}

	public void testTutorialFunction()
	{
		//resources.testDataChange ();

		if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) { //if team one is on a type one obstacle
			teamTutorialObstacles [teamTutorialCurrentObstacle].resourcesLeft -= resources.teamTutorialResourceOneDifference; // subtract what is new from what is left
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceOneDifference, 1));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceTwoDifference, 2));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceThreeDifference, 3));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceFourDifference, 4));
		}

		if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) { //if team one is on a type two obstacle
			teamTutorialObstacles [teamTutorialCurrentObstacle].resourcesLeft -= resources.teamTutorialResourceTwoDifference; // subtract what is new from what is left
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceOneDifference, 1));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceTwoDifference, 2));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceThreeDifference, 3));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceFourDifference, 4));
		}

		if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) { //if team one is on a type three obstacle
			teamTutorialObstacles [teamTutorialCurrentObstacle].resourcesLeft -= resources.teamTutorialResourceThreeDifference; // subtract what is new from what is left
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceOneDifference, 1));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceTwoDifference, 2));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceThreeDifference, 3));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceFourDifference, 4));
		}

		if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) { //if team one is on a type four obstacle
			teamTutorialObstacles [teamTutorialCurrentObstacle].resourcesLeft -= resources.teamTutorialResourceFourDifference; // subtract what is new from what is left
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceOneDifference, 1));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceTwoDifference, 2));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceThreeDifference, 3));
			StartCoroutine(spawnResource(0 , resources.teamTutorialResourceFourDifference, 4));
		}
	}

	public void doObstacleMath()
	{
		if (teamOneCurrentObstacle < teamOneObstacles.Length) {
			if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) { //if team one is on a type one obstacle
				teamOneObstacles [teamOneCurrentObstacle].resourcesLeft -= resources.teamOneResourceOneDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(1 , resources.teamOneResourceOneDifference, 1));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceTwoDifference, 2));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceThreeDifference, 3));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceFourDifference, 4));
			}

			if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) { //if team one is on a type two obstacle
				teamOneObstacles [teamOneCurrentObstacle].resourcesLeft -= resources.teamOneResourceTwoDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(1 , resources.teamOneResourceOneDifference, 1));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceTwoDifference, 2));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceThreeDifference, 3));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceFourDifference, 4));
			}

			if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) { //if team one is on a type three obstacle
				teamOneObstacles [teamOneCurrentObstacle].resourcesLeft -= resources.teamOneResourceThreeDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(1 , resources.teamOneResourceOneDifference, 1));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceTwoDifference, 2));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceThreeDifference, 3));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceFourDifference, 4));
			}

			if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) { //if team one is on a type four obstacle
				teamOneObstacles [teamOneCurrentObstacle].resourcesLeft -= resources.teamOneResourceFourDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(1 , resources.teamOneResourceOneDifference, 1));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceTwoDifference, 2));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceThreeDifference, 3));
				StartCoroutine(spawnResource(1 , resources.teamOneResourceFourDifference, 4));
			}
		}


		if (teamTwoCurrentObstacle < teamTwoObstacles.Length) {
			if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) { //if team two is on a type one obstacle
				teamTwoObstacles [teamTwoCurrentObstacle].resourcesLeft -= resources.teamTwoResourceOneDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceOneDifference, 1));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceTwoDifference, 2));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceThreeDifference, 3));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceFourDifference, 4));
			}

			if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) { //if team two is on a type two obstacle
				teamTwoObstacles [teamTwoCurrentObstacle].resourcesLeft -= resources.teamTwoResourceTwoDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceOneDifference, 1));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceTwoDifference, 2));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceThreeDifference, 3));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceFourDifference, 4));
			}

			if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) { //if team two is on a type three obstacle
				teamTwoObstacles [teamTwoCurrentObstacle].resourcesLeft -= resources.teamTwoResourceThreeDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceOneDifference, 1));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceTwoDifference, 2));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceThreeDifference, 3));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceFourDifference, 4));
			}

			if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) { //if team two is on a type four obstacle
				teamTwoObstacles [teamTwoCurrentObstacle].resourcesLeft -= resources.teamTwoResourceFourDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceOneDifference, 1));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceTwoDifference, 2));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceThreeDifference, 3));
				StartCoroutine(spawnResource(2 , resources.teamTwoResourceFourDifference, 4));
			}
		}

		if (teamThreeCurrentObstacle < teamThreeObstacles.Length) 
		{
			if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) { //if team three is on a type one obstacle
				teamThreeObstacles [teamThreeCurrentObstacle].resourcesLeft -= resources.teamThreeResourceOneDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceOneDifference, 1));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceTwoDifference, 2));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceThreeDifference, 3));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceFourDifference, 4));
			}

			if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) { //if team three is on a type two obstacle
				teamThreeObstacles [teamThreeCurrentObstacle].resourcesLeft -= resources.teamThreeResourceTwoDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceOneDifference, 1));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceTwoDifference, 2));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceThreeDifference, 3));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceFourDifference, 4));
			}

			if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) { //if team three is on a type three obstacle
				teamThreeObstacles [teamThreeCurrentObstacle].resourcesLeft -= resources.teamThreeResourceThreeDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceOneDifference, 1));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceTwoDifference, 2));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceThreeDifference, 3));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceFourDifference, 4));
			}

			if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) { //if team three is on a type four obstacle
				teamThreeObstacles [teamThreeCurrentObstacle].resourcesLeft -= resources.teamThreeResourceFourDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceOneDifference, 1));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceTwoDifference, 2));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceThreeDifference, 3));
				StartCoroutine(spawnResource(3 , resources.teamThreeResourceFourDifference, 4));
			}
		}

		if (teamFourCurrentObstacle < teamFourObstacles.Length) {
			if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) { //if team four is on a type one obstacle
				teamFourObstacles [teamFourCurrentObstacle].resourcesLeft -= resources.teamFourResourceOneDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(4 , resources.teamFourResourceOneDifference, 1));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceTwoDifference, 2));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceThreeDifference, 3));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceFourDifference, 4));
			}

			if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) { //if team four is on a type two obstacle
				teamFourObstacles [teamFourCurrentObstacle].resourcesLeft -= resources.teamFourResourceTwoDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(4 , resources.teamFourResourceOneDifference, 1));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceTwoDifference, 2));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceThreeDifference, 3));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceFourDifference, 4));
			}

			if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) { //if team four is on a type three obstacle
				teamFourObstacles [teamFourCurrentObstacle].resourcesLeft -= resources.teamFourResourceThreeDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(4 , resources.teamFourResourceOneDifference, 1));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceTwoDifference, 2));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceThreeDifference, 3));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceFourDifference, 4));
			}

			if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) { //if team four is on a type four obstacle
				teamFourObstacles [teamFourCurrentObstacle].resourcesLeft -= resources.teamFourResourceFourDifference; // subtract what is new from what is left
				StartCoroutine(spawnResource(4 , resources.teamFourResourceOneDifference, 1));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceTwoDifference, 2));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceThreeDifference, 3));
				StartCoroutine(spawnResource(4 , resources.teamFourResourceFourDifference, 4));
			}
		}
	}


	public IEnumerator spawnResource(int team, float amount, int type)
	{
		for (int i = 0; i < amount; i++) 
		{
			amount = Mathf.Floor (amount);
			tempObject = Instantiate (resourceArray [type], resourceSpawners [team].GetComponent<Transform> ().position, Quaternion.identity);
			tempObject.GetComponent<MoveTowards> ().target = playerTargets [team];
			if (type == 1) 
			{
				tempObject.GetComponent<MoveTowards> ().resourceType = 1;
			}

			if (type == 2) 
			{
				tempObject.GetComponent<MoveTowards> ().resourceType = 2;
			}

			if (type == 3) 
			{
				tempObject.GetComponent<MoveTowards> ().resourceType = 3;
			}

			if (type == 4) 
			{
				tempObject.GetComponent<MoveTowards> ().resourceType = 4;
			}

			if (team == 0) 
			{
				tempObject.GetComponent<MoveTowards> ().teamNumber = 0;

				if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 1;
				}

				if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 2;
				}

				if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 3;
				}

				if (teamTutorialObstacles [teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 4;
				}

			}

			if (team == 1) 
			{
				tempObject.GetComponent<MoveTowards> ().teamNumber = 1;

				if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 1;
				}

				if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 2;
				}

				if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 3;
				}

				if (teamOneObstacles [teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 4;
				}
			}

			if (team == 2) 
			{
				tempObject.GetComponent<MoveTowards> ().teamNumber = 2;



				if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 1;
				}

				if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 2;
				}

				if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 3;
				}

				if (teamTwoObstacles [teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 4;
				}
			}

			if (team == 3) 
			{
				tempObject.GetComponent<MoveTowards> ().teamNumber = 3;





				if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 1;
				}

				if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 2;
				}

				if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 3;
				}

				if (teamThreeObstacles [teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 4;
				}
			}

			if (team == 4) 
			{
				tempObject.GetComponent<MoveTowards> ().teamNumber = 4;
				if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 1;
				}

				if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 2;
				}

				if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 3;
				}

				if (teamFourObstacles [teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4) 
				{
					tempObject.GetComponent<MoveTowards> ().teamWantedResourceType = 4;
				}
			}
			yield return new WaitForSeconds (resourceSendTime);
		}
	}
}
