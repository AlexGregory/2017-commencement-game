﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireworks : MonoBehaviour 
{
	private ParticleSystem ps;
	public ParticleSystem.MainModule main;
	public Color color;
	public float timer = 0;
	public float timerTime = 2.0f;

	// Use this for initialization
	void Start () 
	{
		ps = this.GetComponent<ParticleSystem> ();
		main = this.GetComponent<ParticleSystem> ().main;
		main.startColor = color;


	}

	// Update is called once per frame
	void Update () 
	{
		if (timer < timerTime) 
		{
			timer += Time.deltaTime;
		} 
		else 
		{
			main.duration =  Random.Range (1.5f, 2.5f);
			timer = 0;
			color = new Color (Random.Range (100, 256) / 255.0f, Random.Range (100, 256) / 255.0f, Random.Range (100, 256) / 255.0f, 1);
			main.startColor = color;
		}
	}
}
