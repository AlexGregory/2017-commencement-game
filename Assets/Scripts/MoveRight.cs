﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRight : MonoBehaviour {
	public int moveSpeed;
	public int team;
	public bool shouldMove = true;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(shouldMove == true)
		{
		this.gameObject.GetComponent<Transform> ().position = new Vector3 (this.gameObject.GetComponent<Transform> ().position.x + moveSpeed  * Time.deltaTime, this.gameObject.GetComponent<Transform> ().position.y, this.gameObject.GetComponent<Transform> ().position.z) ;
		}
	}



	public void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.GetComponent<Obstacle> () != null) 
		{
			shouldMove = false;
		}
	}
}
