﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleScript : MonoBehaviour 
{
	private ParticleSystem ps;
	public ParticleSystem.MainModule main;
	public Color color;
	// Use this for initialization
	void Start () 
	{
		ps = this.GetComponent<ParticleSystem> ();
		main = this.GetComponent<ParticleSystem> ().main;
		main.startColor = color;
	}

	// Update is called once per frame
	void Update () 
	{
		if (ps) 
		{
			if (!ps.IsAlive ()) 
			{
				Destroy (gameObject);

			}
		}
	}
}
