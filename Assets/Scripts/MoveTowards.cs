﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour 
{
	public GameObject target;
	public float moveSpeed;
	public int resourceType;
	public int teamNumber;
	public int teamWantedResourceType;
	public GameObject correctMatch;
	public GameObject wrongMatch;
	public GameObject tempObject;
	public float alpha;
	// Use this for initialization
	void Start () 
	{
		
	}
	public float numberToColor( float number)
	{
		return (number / 255.0f);
	}
	// Update is called once per frame
	void Update () 
	{
		this.gameObject.GetComponent<Transform> ().position = Vector3.MoveTowards (this.gameObject.GetComponent<Transform> ().position, target.GetComponent<Transform> ().position, moveSpeed * Time.deltaTime);

		if (Vector3.Distance (this.gameObject.GetComponent<Transform> ().position, target.GetComponent<Transform> ().position) < 0.1f) 
		{

			//Team Tutorial
			if (teamNumber == 0) 
			{

				if (GameManager._GameManager.teamTutorialTimer >= GameManager._GameManager.timerMaxTime) 
				{
					alpha = 1.0f;
				} 
				else 
				{
					alpha = GameManager._GameManager.teamTutorialTimer / GameManager._GameManager.timerMaxTime;
				}

				if (GameManager._GameManager.teamTutorialCurrentObstacle >= GameManager._GameManager.teamTutorialObstacles.Length) 
				{
					Destroy (this.gameObject);
				}

				else if ( GameManager._GameManager.teamTutorialObstacles [GameManager._GameManager.teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1 && resourceType == 1) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);

					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamTutorialObstacles [GameManager._GameManager.teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2 && resourceType == 2) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamTutorialObstacles [GameManager._GameManager.teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3 && resourceType == 3) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if (GameManager._GameManager.teamTutorialObstacles [GameManager._GameManager.teamTutorialCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4 && resourceType == 4) {
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
					Debug.Log("Create correct match for resource four");
				} 
				else 
				{
					tempObject = Instantiate (wrongMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (216.0f / 255.0f, 48.0f / 255.0f, 48.0f / 255.0f, alpha); // SET ALPHA LATER
				}

			}


			//Team One
			if (teamNumber == 1) 
			{


				if (GameManager._GameManager.teamOneTimer >= GameManager._GameManager.timerMaxTime) 
				{
					alpha = 1.0f;
				} 
				else 
				{
					alpha = GameManager._GameManager.teamOneTimer / GameManager._GameManager.timerMaxTime;
				}

				if (GameManager._GameManager.teamOneCurrentObstacle >= GameManager._GameManager.teamOneObstacles.Length) 
				{
					Destroy (this.gameObject);
				}

				if (GameManager._GameManager.teamOneCurrentObstacle == 4) 
				{
					Destroy (this.gameObject);
				}
				else if ( GameManager._GameManager.teamOneObstacles [GameManager._GameManager.teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1 && resourceType == 1) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamOneObstacles [GameManager._GameManager.teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2 && resourceType == 2) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamOneObstacles [GameManager._GameManager.teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3 && resourceType == 3) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if (GameManager._GameManager.teamOneObstacles [GameManager._GameManager.teamOneCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4 && resourceType == 4) {
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				} 
				else 
				{
					tempObject = Instantiate (wrongMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (216.0f / 255.0f, 48.0f / 255.0f, 48.0f / 255.0f, alpha); // SET ALPHA LATER
				}

			}

			//Team Two
			if (teamNumber == 2) 
			{


				if (GameManager._GameManager.teamTwoTimer >= GameManager._GameManager.timerMaxTime) 
				{
					alpha = 1.0f;
				} 
				else 
				{
					alpha = GameManager._GameManager.teamTwoTimer / GameManager._GameManager.timerMaxTime;
				}

				if (GameManager._GameManager.teamTwoCurrentObstacle >= GameManager._GameManager.teamTwoObstacles.Length) 
				{
					Destroy (this.gameObject);
				}

				if ( GameManager._GameManager.teamTwoObstacles [GameManager._GameManager.teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1 && resourceType == 1) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamTwoObstacles [GameManager._GameManager.teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2 && resourceType == 2) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamTwoObstacles [GameManager._GameManager.teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3 && resourceType == 3) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if (GameManager._GameManager.teamTwoObstacles [GameManager._GameManager.teamTwoCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4 && resourceType == 4) {
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				} 
				else 
				{
					tempObject = Instantiate (wrongMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (216.0f / 255.0f, 48.0f / 255.0f, 48.0f / 255.0f, alpha); // SET ALPHA LATER
				}

			}

			//Team Three
			if (teamNumber == 3) 
			{

				if (GameManager._GameManager.teamThreeTimer >= GameManager._GameManager.timerMaxTime) 
				{
					alpha = 1.0f;
				} 
				else 
				{
					alpha = GameManager._GameManager.teamThreeTimer / GameManager._GameManager.timerMaxTime;
				}

				if (GameManager._GameManager.teamThreeCurrentObstacle >= GameManager._GameManager.teamThreeObstacles.Length) 
				{
					Destroy (this.gameObject);
				}


				if ( GameManager._GameManager.teamThreeObstacles [GameManager._GameManager.teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1 && resourceType == 1) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamThreeObstacles [GameManager._GameManager.teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2 && resourceType == 2) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamThreeObstacles [GameManager._GameManager.teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3 && resourceType == 3) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if (GameManager._GameManager.teamThreeObstacles [GameManager._GameManager.teamThreeCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4 && resourceType == 4) {
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				} 
				else 
				{
					tempObject = Instantiate (wrongMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (216.0f / 255.0f, 48.0f / 255.0f, 48.0f / 255.0f, alpha); // SET ALPHA LATER
				}

			}


			//Team Four
			if (teamNumber == 4) 
			{

				if (GameManager._GameManager.teamFourTimer >= GameManager._GameManager.timerMaxTime) 
				{
					alpha = 1.0f;
				} 
				else 
				{
					alpha = GameManager._GameManager.teamFourTimer / GameManager._GameManager.timerMaxTime;
				}

				if (GameManager._GameManager.teamFourCurrentObstacle >= GameManager._GameManager.teamFourObstacles.Length) 
				{
					Destroy (this.gameObject);
				}

				if ( GameManager._GameManager.teamFourObstacles [GameManager._GameManager.teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource1 && resourceType == 1) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamFourObstacles [GameManager._GameManager.teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource2 && resourceType == 2) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if ( GameManager._GameManager.teamFourObstacles [GameManager._GameManager.teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource3 && resourceType == 3) 
				{
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				}

				else if (GameManager._GameManager.teamFourObstacles [GameManager._GameManager.teamFourCurrentObstacle].resourceType == Obstacle.ResourceType.Resource4 && resourceType == 4) {
					tempObject = Instantiate (correctMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (39.0f / 255.0f, 165.0f / 255.0f, 64.0f / 255.0f, alpha); // SET ALPHA LATER
				} 
				else 
				{
					tempObject = Instantiate (wrongMatch, this.GetComponent<Transform> ().position, Quaternion.identity);
					tempObject.GetComponent<ParticleScript> ().color = new Color (216.0f / 255.0f, 48.0f / 255.0f, 48.0f / 255.0f, alpha); // SET ALPHA LATER
				}

			}
			Destroy (this.gameObject);
		}
	}
}
