﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndScene : MonoBehaviour 
{


	public GameObject firstPlace;
	public GameObject secondPlace;
	public GameObject thirdPlace;
	public GameObject fourthPlace;
	public Text winText;

	// Use this for initialization
	void Start () 
	{

		firstPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamOneSprite;
		secondPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamTwoSprite;
		thirdPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamThreeSprite;
		fourthPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamFourSprite;

		if (GameManager._GameManager.fistPlaceTeam == 1) 
		{
			winText.text = "Team One Wins!";
			firstPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamOneSprite;
		}

		if (GameManager._GameManager.fistPlaceTeam == 2) 
		{
			winText.text = "Team Two Wins!";
			firstPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamTwoSprite;
		}

		if (GameManager._GameManager.fistPlaceTeam == 3) 
		{
			winText.text = "Team Three Wins!";
			firstPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamThreeSprite;
		}

		if (GameManager._GameManager.fistPlaceTeam == 4) 
		{
			winText.text = "Team Four Wins!";
			firstPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamFourSprite;
		}





		if (GameManager._GameManager.secondPlaceTeam == 1) 
		{
			secondPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamOneSprite;
		}

		if (GameManager._GameManager.secondPlaceTeam == 2) 
		{
			secondPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamTwoSprite;
		}

		if (GameManager._GameManager.secondPlaceTeam == 3) 
		{
			secondPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamThreeSprite;
		}

		if (GameManager._GameManager.secondPlaceTeam == 4) 
		{
			secondPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamFourSprite;
		}




		if (GameManager._GameManager.thirdPlaceTeam == 1) 
		{
			thirdPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamOneSprite;
		}

		if (GameManager._GameManager.thirdPlaceTeam == 2) 
		{
			thirdPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamTwoSprite;
		}

		if (GameManager._GameManager.thirdPlaceTeam == 3) 
		{
			thirdPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamThreeSprite;
		}

		if (GameManager._GameManager.thirdPlaceTeam == 4) 
		{
			thirdPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamFourSprite;
		}



		if (GameManager._GameManager.fourthPlaceTeam == 1) 
		{
			fourthPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamOneSprite;
		}

		if (GameManager._GameManager.fourthPlaceTeam == 2) 
		{
			fourthPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamTwoSprite;
		}

		if (GameManager._GameManager.fourthPlaceTeam == 3) 
		{
			fourthPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamThreeSprite;
		}

		if (GameManager._GameManager.fourthPlaceTeam == 4) 
		{
			fourthPlace.GetComponent<SpriteRenderer> ().sprite = GameManager._GameManager.teamFourSprite;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
