﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour 
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.GetComponent<MoveRight> () != null) 
		{
			placeTeam (coll.gameObject.GetComponent<MoveRight> ().team, coll.gameObject);
		}
	}


	public void placeTeam(int teamNumber, GameObject student)
	{
		if (teamNumber == 1) //Set that team one has placed in the game manager.
		{
			GameManager._GameManager.teamOnePlaced = true;
		}

		if (teamNumber == 2) //Set that team two has placed in the game manager.
		{
			GameManager._GameManager.teamTwoPlaced = true;
		}

		if (teamNumber == 3) //Set that team three has placed in the game manager.
		{
			GameManager._GameManager.teamThreePlaced = true;
		}

		if (teamNumber == 4) //Set that team four has placed in the game manager.
		{
			GameManager._GameManager.teamFourPlaced = true;
		}

		if (GameManager._GameManager.fistPlaceTeam == 0) 
		{
			GameManager._GameManager.fistPlaceTeam = student.GetComponent<MoveRight> ().team;
		} 
		else if (GameManager._GameManager.secondPlaceTeam == 0) 
		{
			GameManager._GameManager.secondPlaceTeam = student.GetComponent<MoveRight> ().team;
		}
		else if (GameManager._GameManager.thirdPlaceTeam == 0) 
		{
			GameManager._GameManager.thirdPlaceTeam = student.GetComponent<MoveRight> ().team;
		}
		else
		{
			GameManager._GameManager.fourthPlaceTeam = student.GetComponent<MoveRight> ().team;
		}
	}
}
