﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Obstacle : MonoBehaviour 
{
	public enum ResourceType { Resource1, Resource2, Resource3, Resource4};
	public ResourceType resourceType;
	public int totalResourceAmountNeeded;
	public int resourcesLeft;

	public Image completionBar; // bar to show fill progress.
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
}
